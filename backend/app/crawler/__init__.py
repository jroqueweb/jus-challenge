from app.crawler.base import NotFoundException
from .esaj import  TJMSProcessExtractor, TJSPProcessExtractor


map_tj = dict(
    tjsp=TJSPProcessExtractor,
    tjms=TJMSProcessExtractor
)


def search_process(local, code):

    tj_class = map_tj.get(local)
    
    if not tj_class:
        raise NotFoundException()
    
    tj_obj = tj_class(code)
    return tj_obj.get_data()

