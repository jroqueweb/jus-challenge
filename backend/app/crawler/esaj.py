import requests
from bs4 import BeautifulSoup
from .base import AbstractProcessExtractor, ProcessObject, NotFoundException
from dateutil import parser as date_parser
import bleach
import re

class ESAJProcessExtractor(AbstractProcessExtractor):

    base_url = ''
    search_endpoint = '/search.do'
    form_endpoint = '/open.do'
    local_pesquisa = None
    date_regex = re.compile(r'([^\d\:\/\ ]+)')

    terms_mappers = {
        'process_type': []
    }

    key_map = {
        'Classe:': 'process_type',
        'Assunto:': 'subject',
        'Distribuição:': 'date',
        'Juiz:': 'judge',
        'Área:': 'area',
        'Valor da ação:': 'amount',
        'Autora:': 'author',
        'Réu:': 'indictee',
        'Reqte:': 'author',
        'Reqdo:': 'indictee'
    }
    
    def __init__(self, process_number):
        self.process = ProcessObject(code=process_number)

    def retrieve_html(self):

        query = {
            'dadosConsulta.localPesquisa.cdLocal': self.local_pesquisa,
            'cbPesquisa': 'NUMPROC',
            'dadosConsulta.tipoNuProcesso': 'SAJ',
            'dadosConsulta.valorConsulta': self.process.code
        }

        session = requests.Session()
        url = '{}{}'.format(self.base_url, self.search_endpoint)

        response = session.get(url, params=query, allow_redirects=True, verify=False)
        response.raise_for_status()

        return response.text

    def parse_header_data(self, soup):

        # check if exists table for process header 
        has_header = soup.select('div > .secaoFormBody')
        
        if not has_header:
            raise NotFoundException("Process not found")

        header = has_header[-1]
        items = header.select('> tr')
        for item in items:
            columns = item.select('> td')
            columns_qtde = len(columns)

            if len(columns) != 2:
                continue
                
            key_name = ''
            column_label = columns[0]
            column_value = columns[1]

            has_label = column_label.select_one('label')
            if not has_label:

                try:
                    label_in_value = column_value.select_one('span.labelClass')
                    label_text = label_in_value.text.strip()
                    full_text = str(column_value.text.strip())
                    key_name = str(label_text)
                    value = full_text.replace(label_text, '').strip()
                    
                except Exception as e:
                    continue

            else:

                key_name = has_label.text.strip()
                value = str(column_value.select_one('span').text.strip())


            key = self.key_map.get(key_name)

            if not key:
                # Todo: inserir esse erro no log
                continue
            
            if key == 'amount':
                # If amount, remove currency reference and extra space
                value = value.replace(' ', '').replace('R$', '')
            
            if key == 'date':
                # If date, clean date format and parse
                value = date_parser.parse(self.date_regex.sub('', value))

            if hasattr(self.process, key):
                setattr(self.process, key, value)

    def parse_parts(self, soup):

        parts = soup.select('#tableTodasPartes tr')
        process_parts = []

        for part in parts:
            columns = part.select('> td')

            if len(columns) != 2:
                # Todo: add to log. A problem with data
                continue

            col_group = columns[0]
            col_parts = columns[1]

            key_group = self.key_map.get('{}'.format(col_group.text.strip()))

            if not key_group:
                # todo: add to log. key not mapped
                continue

            inner_text_parts = str(col_parts.decode_contents()).split('<br/>')

            parts_list = [
                bleach.clean(l, strip=True).strip()
                .replace('\xa0', ' ').replace('\t', '').replace('\n', '') for l in inner_text_parts
            ]

            process_parts.append({
                'group': key_group,
                'parts': parts_list
            })

        self.process.parts = process_parts


    def parse_moviments(self, soup):
        
        moviments = soup.select('#tabelaTodasMovimentacoes tr')

        process_moviments = []

        for moviment in moviments:
            columns = moviment.select('> td')

            if len(columns) != 3:
                # Todo: add to log. A problem with data
                continue

            col_date = columns[0]
            col_text = columns[-1]

            moviment_date = date_parser.parse(col_date.text.strip())
            mov_lines = str(col_text.text).replace('\t', '').strip().splitlines()
            moviment_text_lines = [l for l in mov_lines if str(l).strip() != '']
            process_moviments.append({
                'date': col_date.text.strip(),
                'lines': moviment_text_lines
            })
        
        self.process.moviments = process_moviments

    def get_data(self):

        html = self.retrieve_html()
        soup = BeautifulSoup(html, 'html.parser')

        self.parse_header_data(soup)
        self.parse_moviments(soup)
        self.parse_parts(soup)

        return self.process.__dict__


class TJMSProcessExtractor(ESAJProcessExtractor):
    
    base_url = 'https://www.tjms.jus.br/cpopg5'
    local_pesquisa = '1'

class TJSPProcessExtractor(ESAJProcessExtractor):
    
    base_url = 'https://esaj.tjsp.jus.br/cpopg'
    local_pesquisa = '-1'
