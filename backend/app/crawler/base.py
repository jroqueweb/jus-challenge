from abc import ABC, abstractmethod


class NotFoundException(Exception):
    pass


class ProcessObject:
    
    def __init__(
        self, code=None, process_type=None, area=None, subject=None, date=None, 
        judge=None, amount=None, parts=[], moviments=[]):
        
        self.code = code
        self.process_type = process_type
        self.area = area
        self.subject = subject
        self.date = date
        self.judge = judge
        self.amount = amount
        self.parts = parts
        self.moviments = moviments

class AbstractProcessExtractor(ABC):    
    pass
