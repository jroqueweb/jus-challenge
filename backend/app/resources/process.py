from flask.views import MethodView
from flask import jsonify, current_app, abort
from app.crawler import search_process
from app.crawler.base import NotFoundException


class ProcessResource(MethodView):
    
    def get(self, code, local):

        try:
            process = search_process(local, code)
            return jsonify(process), 200
        except NotFoundException:
            return abort(404)
        except Exception:
            return abort(500)

process_view = ProcessResource.as_view('process')
