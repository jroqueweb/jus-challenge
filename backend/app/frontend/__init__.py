from flask.views import MethodView
from flask import render_template, request, current_app
from app.crawler.base import NotFoundException
from app.crawler import search_process
import logging


logging.basicConfig(level=logging.DEBUG)


class SearchView(MethodView):

    def get_default_data(self):
        data = request.form.items()

        tribunais = [
            {
                "code": "tjms", 
                "name": "TJMS",
            },
            {
                "code": "tjsp", 
                "name": "TJSP"
            }
        ]

        return {
            "tribunais": tribunais,
            "formdata": dict(data)
        }

    def get(self):
        content = self.get_default_data()
        return render_template('index.html', **content)
    
    def post(self):

        data = dict(request.form.items())

        content = self.get_default_data()

        try:
            process = search_process(data.get('local'), data.get('code'))
            content['process'] = process
            return render_template('index.html', **content)
        except NotFoundException:
            content['error'] = 404
            return render_template('index.html', **content)
        except Exception as e:
            content['error'] = 500
            return render_template('index.html', **content)


search_view = SearchView.as_view('search')
