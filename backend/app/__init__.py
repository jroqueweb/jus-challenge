import os
from flask import Flask
import requests_cache
from datetime import timedelta
from .resources import process
from .frontend import search_view


class AppConfig:
    
    DEBUG = os.getenv('APP_DEBUG', True)
    CSRF_ENABLED = False
    SECRET = os.getenv('APP_SECRET_KEY', False)
    ENV = os.getenv('APP_ENV', 'development')


def create_app(testing_mode=False):

    app = Flask(__name__, template_folder='www/templates', static_folder='www/assets', static_url_path='/static')
    app.config.from_object(AppConfig)

    expire_after = timedelta(hours=1)
    requests_cache.install_cache('tj_requests_cache', expire_after=expire_after)

    # Testing mode rules
    if(testing_mode):
        app.config.TESTING = True
        app.config.DEBUG = True

    app.add_url_rule('/process/<code>/<local>', view_func=process.process_view, methods=['GET'])
    app.add_url_rule('/', view_func=search_view, methods=['GET', 'POST'])
    
    return app
