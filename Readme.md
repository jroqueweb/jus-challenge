Desafio JusBrasil - Backend Enginner
===========


* Desafio: (https://gist.github.com/gabrielpjordao/3dd01ca89678c89ef3ad6ec021b79665)


Como rodar o projeto com Docker:
----------

1. Subir o container com o docker-compose: docker-compose up
2. Acessar no browser a url http://localhost:8000

Como rodar o projeto sem Docker:
----------
1. Ter o pipenv instalado e estar na pasta /backend
2. Instalar as dependencias do projeto: pipenv install
3. Carregar o virtualenv: pipenv shell
4. Rodar o serviço: python run.py
2. Acessar no browser a url http://localhost:5000